import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from scipy.integrate import quad

import inspect

a_ = -1
b_ = 1
n = 100
h = (b_ - a_) / n
time_grid = np.linspace(a_, b_, n)


def phi(k, x):
    if time_grid[k - 1] <= x <= time_grid[k]:
        return (x - time_grid[k - 1]) / (time_grid[k] - time_grid[k - 1])
    if time_grid[k] <= x <= time_grid[k + 1]:
        return (time_grid[k + 1] - x) / (time_grid[k + 1] - time_grid[k])

    return 0


def phi_d(k, x):
    if time_grid[k - 1] <= x <= time_grid[k]:
        return 1 / (time_grid[k] - time_grid[k - 1])
    if time_grid[k] <= x <= time_grid[k + 1]:
        return 1 / (time_grid[k] - time_grid[k + 1])

    return 0


def a(x):
    return (2 + x) / (3 + x)


def c(x):
    return 1 + np.sin(x)


def f(x):
    return 1 - x


def initA(n: int):
    A = np.zeros((n - 2, n - 2))

    for i in range(1, n - 1):
        deltai = [time_grid[i - 1], time_grid[i + 1]]
        for j in range(1, n - 1):
            deltaj = [time_grid[j - 1], time_grid[j + 1]]
            deltaij = [max(deltai[0], deltaj[0]), min(deltai[1], deltaj[1])]
            ans = 0
            if abs(i - j) <= 1:
                integrand = lambda x: a(x) * phi_d(i, x) * phi_d(j, x)
                ans, err = quad(integrand, deltaij[0], deltaij[1])

            A[i - 1][j - 1] = ans

    return A


def initC(n: int):
    C = np.zeros((n - 2, n - 2))

    for i in range(1, n - 1):
        deltai = [time_grid[i - 1], time_grid[i + 1]]
        for j in range(1, n - 1):
            deltaj = [time_grid[j - 1], time_grid[j + 1]]
            deltaij = [max(deltai[0], deltaj[0]), min(deltai[1], deltaj[1])]
            ans = 0
            if abs(i - j) <= 1:
                integrand = lambda x: a(x) * phi(i, x) * phi(j, x)
                ans, err = quad(integrand, deltaij[0], deltaij[1])

            C[i - 1][j - 1] = ans

    return C


def initf(n: int):
    f = np.zeros(n - 2)
    for i in range(1, n - 1):
        deltai = [time_grid[i - 1], time_grid[i]]
        integrand = lambda x: a(x) * phi(i, x)
        ans, err = quad(integrand, deltai[0], deltai[1])
        f[i - 1] = ans

    return f


def U(prev: float, sigma: float, D_inv: np.array, K: np.array, f: np.array):
    return prev - sigma * np.matmul(D_inv, (np.matmul(K, prev) - f))


if __name__ == '__main__':
    A = initA(n)
    C = initC(n)
    f = initf(n)
    # print('A =\n{},\nC =\n{},\nf =\n{}\n'.format(A, C, f))

    K = A + C
    # print('K =\n{}\n'.format(K))

    # D = np.diag(np.diag(K))
    # print('D =\n{}\n'.format(D))

    D_inv = np.linalg.inv(np.diag(np.diag(K)))
    # print(('D1 =\n{}\n'.format(D_inv)))

    # print('D*D_inv =\n{},\nD_inv*D =\n{}\n'.format(D * D_inv, D_inv * D))

    u = np.zeros(n - 2)
    iter = 100000
    while iter:
        prev = u
        u = U(prev=prev, sigma=1, D_inv=D_inv, K=K, f=f)
        iter -= 1

    # print(time_grid)
    # print(u)
    sns.lineplot(x=time_grid[2:], y=u)
    plt.savefig(fname='5_algo', dpi=300)
    plt.show()
    exit(0)
