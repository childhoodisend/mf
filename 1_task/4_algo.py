from scipy.sparse import diags
from scipy.linalg import block_diag
from scipy.integrate import quad
import numpy as np

n = 4
a_ = -1
b_ = 1
h = (b_ - a_) / n
H = n * h
time_grid = np.linspace(a_, b_, n)


def phi(j, x):
    if time_grid[j - 1] <= x <= time_grid[j]:
        return (x - time_grid[j - 1]) / (time_grid[j] - time_grid[j - 1])
    if time_grid[j] <= x <= time_grid[j + 1]:
        return (time_grid[j + 1] - x) / (time_grid[j + 1] - time_grid[j])

    return 0


def phi_d(j, x):
    if time_grid[j - 1] <= x <= time_grid[j]:
        return 1 / (time_grid[j] - time_grid[j - 1])
    if time_grid[j] <= x <= time_grid[j + 1]:
        return 1 / (time_grid[j] - time_grid[j + 1])

    return 0


# TODO phi_H, phi_Hd
def phi_H(r, x):
    return 0


def phi_Hd(r, x):
    return 0


def psi(i: int, x):
    if 1 <= i <= n * (n - 1):
        j = i + (i / n - 1)
        return phi(j, x)
    else:
        r = i - n * (n - 1)
        return phi_H(r, x)


def psi_d(i: int, x):
    if 1 <= i <= n * (n - 1):
        j = i + (i / n - 1)
        return phi_d(j, x)
    else:
        r = i - n * (n - 1)
        return phi_Hd(r, x)


def U(prev: float, sigma: float, D_inv: np.array, K: np.array, f: np.array):
    return prev - sigma * np.matmul(D_inv, (np.matmul(K, prev) - f))


def a(x):
    return (2 + x) / (3 + x)


a_mean = np.mean(a(time_grid))
print('a_mean = {}\n'.format(a_mean))

k = np.array([-np.ones(n - 1), 2 * np.ones(n), -np.ones(n - 1)], dtype=object)
k[1][0] = k[1][-1] = 1
offset = [-1, 0, 1]
delta = diags(k, offset).toarray()
# print(delta)
delta_h = 1 / h * delta
delta_H = 1 / H * delta
print(delta_h)
print(delta_H)

blocks = [delta_h for i in range(n)]
blocks.append(delta_H)
delta_Hh = a_mean * block_diag(*blocks)
print('delta_Hh = \n {}'.format(delta_Hh))

# TODO D_inv is a singular
# ~~~~~~~~~~~~~~~~~~calculate D_inv~~~~~~~~~~~~~~~~~~
D_inv = np.linalg.inv(delta_Hh)
print('D_inv = \n {}'.format(D_inv))

# ~~~~~~~~~~~~~~~~~~calculate f~~~~~~~~~~~~~~~~~~~~~~
f = np.zeros(n - 1)
for i in range(1, n):
    deltai = [time_grid[i - 1], time_grid[i]]
    integrand = lambda x: a(x) * psi(i, x)
    ans, err = quad(integrand, deltai[0], deltai[1])
    f[i - 1] = ans

# ~~~~~~~~~~~~~~~~~~calculate K~~~~~~~~~~~~~~~~~~~~~~


# ~~~~~~~~~~~~~~~~~~calculate u~~~~~~~~~~~~~~~~~~~~~~
u = np.zeros()
