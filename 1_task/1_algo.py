import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

n = 100
a = -1
b = 1
h = (b - a) / n


def p(x):
    return (2 + x) / (3 + x)


def r(x):
    return 1 + np.sin(x)


def q(x):
    return 0


def f(x):
    return 1 - x


def A(time: np.array) -> np.array:
    return np.array(-p(time - 0.5) / (h ** 2) - q(time) / (2 * h))


def B(time) -> np.array:
    return np.array(-(p(time + 0.5) + p(time - 0.5)) / (h ** 2) - r(time))


def C(time: np.array) -> np.array:
    return np.array(-p(time + 0.5) / (h ** 2) + q(time) / (2 * h))


def G(time: np.array) -> np.array:
    return np.array(f(time))


"""
progonka method
"""


def progonka(n: int, y: np.array, G: np.array, A: np.array, B: np.array, C: np.array, s: np.array, t: np.array):
    s[0] = C[0] / B[0]
    t[0] = -G[0] / B[0]

    for i in range(1, n + 1, 1):
        s[i] = C[i] / (B[i] - A[i] * s[i - 1])
        t[i] = (A[i] * t[i - 1] - G[i]) / (B[i] - A[i] * s[i - 1])

    y[n] = t[n]

    for i in range(n - 1, 0, -1):
        y[i] = s[i] * y[i + 1] + t[i]


if __name__ == '__main__':
    time_grid = np.linspace(a, b, n + 1)
    G = G(time_grid)
    A = A(time_grid)
    B = B(time_grid)
    C = C(time_grid)

    t = np.zeros(n + 1)
    s = np.zeros(n + 1)
    y = np.zeros(n + 1)
    progonka(n=n, y=y, G=G, A=A, B=B, C=C, s=s, t=t)

    sns.lineplot(x=time_grid, y=y)
    plt.savefig(fname='1_algo', dpi=300)
    plt.show()
